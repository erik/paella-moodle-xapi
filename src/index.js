export default function getXAPIMoodlePluginsContext() {
    return require.context("./plugins", true, /\.js/)
}
