/**
 * Paella plugin that emits selected media events to parent windows (when embedded as iframe)
 * in order to generate xAPI statements from it.
 *
 * @copyright   2023 Erik Schnell, Sven Judel
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

import {EventLogPlugin, Events} from "paella-core";

// Prefix for all messages sent to parent windows.
// The counterpart knows the same prefix and ignores messages not starting with it.
const PAELLA_EVENT_MAGIC_PREFIX = 'Sxl6y92idS420qk9ysqtc8s3';

export default class MoodleXAPILoggingPlugin extends EventLogPlugin {

    /**
     * Plugin identifier as used in Paellas config.json.
     */
    getName() {
        return "de.rwth-aachen.lufgi9-learntech.paella.xAPI";
    }

    /**
     * Returns the events for which onEvent should be called.
     */
    get events() {
        return Object.values(Events);
    }


    /**
     * Called when a Paella related event occurs.
     * */
    async onEvent(event, params) {
        try {
            switch (event) {
                case Events.PLAYER_LOADED:
                    this.registerEventListeners();
                    break;
            }
        } catch (e) {
            console.error(e);
            return;
        }
    }

    /**
     * List of events that are listened to.
     * */
    mediaEvents = ['play', 'pause', 'end', 'ratechange', 'volumechange', 'seeked'];

    /**
     * List of keys in the media element that should be serialized.
     * */
    mediaAttributes = ['currentTime', 'currentSrc', 'paused', 'volume', 'playbackRate', 'tagName', 'title', 'muted'];

    /**
     * Keep track of media states. This is for example required
     * to get the previous position in a medium after a "seeked" event occurred.
     * */
    mediaStates = [];

    /**
     * Takes a video or audio element and returns an object
     * containing all attributes which are defined in this.mediaAttributes
     * and thus making the media elements JSON-serializable.
     */
    serializeMedium(element) {
        return Object.fromEntries(this.mediaAttributes.map(k => [k, element[k]]));
    }

    /**
     * Register event listeners on all video and audio element
     * which are declared in this.mediaEvents and notify parent window.
     */
    registerEventListeners() {
        let elements = [...document.querySelectorAll('video, audio')];

        window.parent?.postMessage(PAELLA_EVENT_MAGIC_PREFIX + JSON.stringify({
            elements: elements.map(m => this.serializeMedium(m))
        }));

        for (let i = 0; i < elements.length; i++) {

            elements[i].addEventListener('timeupdate', () => {
                setTimeout(() => this.mediaStates[i] = this.serializeMedium(elements[i]));
            });

            for (let eventName of this.mediaEvents) {
                elements[i].addEventListener(eventName, () => {
                    window.parent?.postMessage(PAELLA_EVENT_MAGIC_PREFIX + JSON.stringify({
                        event: eventName,
                        currentState: this.serializeMedium(elements[i]),
                        previousState: this.mediaStates[i],
                        index: i,
                        pagePrefix: location.protocol + '//' + location.host
                    }));

                    this.mediaStates[i] = this.serializeMedium(elements[i]);
                });
            }
        }
    }

    /**
     * Do additional setup if required.
     * Called by Paella if the plugin is enabled.
     */
    load() {
    }
}
