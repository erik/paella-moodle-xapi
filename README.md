# Paella Player xAPI Moodle Plugin

This is a plugin for Paella Player 7 that emits media events to embedding parent windows using the `window.postMessage` function.

The counterpart to read the events and generate xAPI statements for it is contained in the [Moodle xAPI Logging plugin](https://git.rwth-aachen.de/sven.judel/xapi_logging), so both are required in order to work.

## Build

The paella player bundle needs to be rebuilt in order to integrate plugins. It is assumed that you already installed the _mod_opencast_ activity in your Moodle instance.

```shell
cd mod/opencast/paella # relative to moodle path
npm install # install build dependencies
npm i git+https://git.rwth-aachen.de/erik/paella-moodle-xapi.git # install xAPI plugin
```

Then locate the plugin context array in `mod/opencast/paella/player.js`, add the marked lines:

```javascript
import getXAPIMoodlePluginsContext from "paella-moodle-xapi"; // <---- add import

// ...

    {
        // ...
        customPluginContext: [
          getBasicPluginContext(),
          getSlidePluginContext(),
          getZoomPluginContext(),
          getUserTrackingPluginsContext(),
          getXAPIMoodlePluginsContext() // <---- add this line
        ]
        // ...
    }
```

Afterwards create a bundle using

```shell
npx webpack
```

Please be patient, this takes up to a minute.

Now you can tweak _mod/opencast/config.json_ and insert
```json
{
  // ...
  "de.rwth-aachen.lufgi9-learntech.paella.xAPI": {
    "enabled": true
  }
  // ...
}
```
into the plugins section.
