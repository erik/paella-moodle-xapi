const path = require('path');
const config = require('./webpack.common');

config.entry = './src/index.js';
config.output = {
	path: path.join(__dirname, "dist"),
	filename: 'paella-moodle-xapi.js',
	library: 'paella-moodle-xapi',
	libraryTarget: 'umd'
};

module.exports = config;